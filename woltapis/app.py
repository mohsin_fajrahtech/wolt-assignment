import pandas as pd
import numpy as np
from datetime import datetime

from chalice import Chalice

app = Chalice(app_name='woltapis')


@app.route('/',methods=['POST'])
def index():
    request_body = app.current_request.json_body
    days = list(request_body.keys())
    days_closed = list(request_body.keys())

    days_map = {'monday':1, 'tuesday':2, 'wenessday':3, 'thursday':4, 'friday':5, 'saturday':6, 'sunday':7}
    inv_days_map = {v: k for k, v in days_map.items()}

    dfs = []
    for day in days:
        df = pd.json_normalize(request_body, record_path=day)
        df['day'] = days_map[day]
        if not df.empty:
            days_closed.remove(day)
            dfs.append(df)
    data_df = pd.concat(dfs, ignore_index=True)

    result = ''
    data_df = data_df.sort_values(by=['day', 'value'])

    current_day = 0
    for _, g in data_df.groupby(np.arange(len(data_df)) // 2):
        
        if current_day != g['day'].values[0]:
            current_day = g['day'].values[0]
            result += '\n' + inv_days_map[g['day'].values[0]].capitalize() + ' '
        group_start_time = datetime.utcfromtimestamp(g.value.values[0]).strftime('%I:%M %p')
        group_end_time = datetime.utcfromtimestamp(g.value.values[1]).strftime('%I:%M %p')
        result += f'{group_start_time} - {group_end_time} '

    for day in days_closed:
        result += '\n' + f'{day.capitalize()}: Closed'
        
    # return result
    return {'result': result}




# The view function above will return {"hello": "world"}
# whenever you make an HTTP GET request to '/'.
#
# Here are a few more examples:
#
# @app.route('/hello/{name}')
# def hello_name(name):
#    # '/hello/james' -> {"hello": "james"}
#    return {'hello': name}
#
# @app.route('/users', methods=['POST'])
# def create_user():
#     # This is the JSON body the user sent in their POST request.
#     user_as_json = app.current_request.json_body
#     # We'll echo the json body back to the user in a 'user' key.
#     return {'user': user_as_json}
#
# See the README documentation for more examples.
#
