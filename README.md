# README #

This README would try to explain the environment setup part of the assignment and try to answer the questions asked in the document.

### Topics Covered? ###

* Whats Serverless? 
* Why Serverless?
* Challice
* Why Challice?
* Deployment Instructions
* Part 2

### Whats Serverless? ###

Serverless computing (or serverless for short), is an execution model where the cloud provider (AWS, Azure, or Google Cloud) is responsible for executing a piece of code by dynamically allocating the resources. And only charging for the amount of resources used to run the code. The code is typically run inside stateless containers that can be triggered by a variety of events including http requests, database events, queuing services, monitoring alerts, file uploads, scheduled events (cron jobs), etc. The code that is sent to the cloud provider for execution is usually in the form of a function. Hence serverless is sometimes referred to as “Functions as a Service” or “FaaS”.

### Why Serverless? ###

* No server management is necessary
* Developers are only charged for the server space they use, reducing cost
* Serverless architectures are inherently scalable
* Quick deployments and updates are possible
* Code can run closer to the end user, decreasing latency

### Challice ###
Chalice is a framework for writing serverless apps in python. It allows you to quickly create and deploy applications that use AWS Lambda. 

### Why Challice? ###
It provides:

* A command line tool for creating, deploying, and managing your app
* A decorator based API for integrating with Amazon API Gateway, Amazon S3, Amazon SNS, Amazon SQS, and other AWS services.
* Automatic IAM policy generation


### Deployment Instructions ###
First we need to create a virtual environment, so easiest way would be to used the requirements.txt file and create a virtual environment from that, such as:
```bash
conda create --name <env> --file requirements.txt
```
Then, simply activate this environment by doing:
```bash
conda activate <env>
```
Now that the environment is set up, all you need to do is run:
```bash
chalice local
```

This will setup a local API. However, same chalice can be used to deploy this api on AWS setting dev/qa/prod environments, by simply running:
```bash
chalice deploy
```

### Part 2 ###
The obvious flaw was that data values was being used to generate json structure. e.g. days information shouldn't be part of the structure. Instead a flatter hierarchi could've been a better, clean and efficient way.

I think following structure could've been used:

{
    "timing":[
        {
            "day": "monday",
            "type": "open",
            "value": <epoch>
        }
    ]
}

Further extension could be made including multiple Restaurant with a similar timing object.